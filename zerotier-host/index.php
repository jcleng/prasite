<?php
global $token, $network;
$token = getenv('TOKEN');
$network = getenv('NETWORK');
$host = getenv('HOST');

if (empty($token) || empty($network) || empty($host)) {
    throw new \Exception('TOKEN or NETWORK or HOST not set.');
}

function get($url)
{
    global $token;
    //初始化
    $curl = curl_init();
    //设置抓取的url
    curl_setopt($curl, CURLOPT_URL, $url);
    //设置头文件的信息作为数据流输出
    curl_setopt($curl, CURLOPT_HEADER, 1);
    curl_setopt($curl, CURLOPT_HTTPHEADER, [
        'Authorization: token ' . $token,
    ]);
    curl_setopt($curl, CURLOPT_HEADER, false);
    //设置获取的信息以文件流的形式返回，而不是直接输出。
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    //执行命令
    $data = curl_exec($curl);
    curl_close($curl);
    return json_decode($data, true);
}
function member()
{
    global $network, $host;
    $url = "https://api.zerotier.com/api/v1/network/{$network}/member";
    $res = get($url);
    if (isset($res['type'])) {
        throw new \Exception('Err ' . $res['type'] . ($res['message'] ?? ''));
    }
    // 格式化数据
    $list = [];
    foreach ($res as $_k => $item) {
        // IP => HOST
        if(!empty($item['config']['ipAssignments'])) {
            foreach ($item['config']['ipAssignments'] as $_k2 => $ip) {
                $list[$ip] = $item['name'] . '.' . $host;
            }
        }
    }
    return $list;
}
$list = member($network);
// var_dump($list);
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="zerotierhost.txt"');
foreach ($list as $ip => $ht) {
    echo $ip . ' ' . $ht . "\n";
}
