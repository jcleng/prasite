<?php
/**
 * 添加更新cloudflare的dns解析
 * @link https://developers.cloudflare.com/api/operations/dns-records-for-a-zone-patch-dns-record
 *
 * @copyright jcleng
 * @author jcleng
 */
class UpdateDns
{
    private $ZONE_ID = 'baidu.com'; // [网站][概述][区域 ID]
    private $x_auth_email = null;
    private $x_auth_key = null; // https://dash.cloudflare.com/profile/api-tokens [API 令牌]
    public function __construct($ZONE_ID, $auth)
    {
        $this->ZONE_ID = $ZONE_ID;
        if (empty($auth['x_auth_email']) || empty($auth['x_auth_key'])) {
            throw new \Exception("auth NOT SET");
        }
        $this->x_auth_email = $auth['x_auth_email'];
        $this->x_auth_key = $auth['x_auth_key'];
    }
    public function update($data)
    {
        try {
            $url = 'https://api.cloudflare.com/client/v4/zones/' . $this->ZONE_ID . '/dns_records';
            return $this->post($url, $data);
        } catch (\Throwable $th) {
            // 已经添加就进行更新
            if (strpos($th->getMessage(), '81057') !== false) {
                $url = 'https://api.cloudflare.com/client/v4/zones/' . $this->ZONE_ID . '/dns_records';
                $dnslist = $this->post($url, $data, 'GET');
                foreach ($dnslist as $_k => $oneconfig) {
                    if ($oneconfig['name'] == $data['name']) {
                        $id = $oneconfig['id'];
                    }
                }
                $url = 'https://api.cloudflare.com/client/v4/zones/' . $this->ZONE_ID . '/dns_records/' . $id;
                return $this->post($url, $data, 'PATCH');
            } else {
                throw $th;
            }
        }
    }
    /**
     * post请求
     *
     * @param string $url
     * @param array $post_data post数据
     * @return mixed
     * @copyright jcleng
     * @author jcleng
     */
    public function post($url, $post_data = [], $type = 'POST', $returnjson = true)
    {
        //初始化
        $curl = curl_init();
        //设置抓取的url
        curl_setopt($curl, CURLOPT_URL, $url);
        //设置头文件的信息作为数据流输出
        curl_setopt($curl, CURLOPT_HEADER, 1);
        //设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        if ($type == 'POST') {
            //设置post方式提交
            curl_setopt($curl, CURLOPT_POST, 1);
        } else {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);
        }

        if (!empty($post_data)) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($post_data));
        }
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'x-auth-email: ' . $this->x_auth_email,
            'Authorization: Bearer ' . $this->x_auth_key,
        ]);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $data = curl_exec($curl);
        curl_close($curl);
        if (!$returnjson) {
            return $data;
        }
        $res_arr = json_decode($data, true);
        if (isset($res_arr['success']) && $res_arr['success'] == false) {
            throw new \Exception($res_arr['errors'][0]['code'] . ' ' . $res_arr['errors'][0]['message']);
        }
        return $res_arr['result'];
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    // 表单
    echo <<<HTML
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>添加更新cloudflare的dns解析</title>
  </head>
  <style>
    label {
        display: block;
    }
    input {
        width: 600px;
        margin-top: 12px;
    }
  </style>
  <body>
    <form action="" method="post">
      <label>
        ZONE_ID:
        <input type="text" name="ZONE_ID" value="339baadd2c5a1d9d16a20f437d46c671"/>
      </label>
      <label>
        x_auth_email:
        <input type="text" name="x_auth_email" value="j.c.leng@foxmail.com" />
      </label>
      <label>
        x_auth_key:
        <input type="text" name="x_auth_key" value=""/>
      </label>
      <label>
        hosturl:
        <input type="text" name="hosturl" value="http://adhome.2011101.xyz:54322/"/>
      </label>
      <input type="submit" value="提交"/>
    </form>
  </body>
</html>

HTML;
    die();
}
//
$ZONE_ID = $_POST['ZONE_ID'];
$x_auth_email = $_POST['x_auth_email'];
$x_auth_key = $_POST['x_auth_key'];
// 数据
$hosturl = $_POST['hosturl'];

if (!empty($hosturl)) {
    $url_data = (new UpdateDns($ZONE_ID, [
        'x_auth_email' => $x_auth_email,
        'x_auth_key' => $x_auth_key,
    ]))->post($hosturl, [], 'GET', false);
    $lines = array_filter(explode("\n", $url_data));
    foreach ($lines as $__k => $onekv) {
        list($host, $name) = explode(" ", $onekv);
        try {
            $api = new UpdateDns($ZONE_ID, [
                'x_auth_email' => $x_auth_email,
                'x_auth_key' => $x_auth_key,
            ]);
            $api->update([
                "type" => "A",
                "name" => $name,
                "content" => $host,
                "ttl" => 60,
                "proxied" => false,
            ]);
        } catch (\Throwable $th) {
            echo $th->getMessage() . PHP_EOL . PHP_EOL . PHP_EOL;
        }
    }
    echo '保存OK';
} else {
    throw new \Exception("请填写数据地址!");
}
