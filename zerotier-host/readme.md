- 获取当前zerotier的网络host

```shell
# https://dash.cloudflare.com/profile/api-tokens [API 令牌]

docker run --restart=always -itd \
-p 54322:80 \
--dns=1.1.1.1 \
-e NETWORK=e3918db4831c3510 \
-e TOKEN=xxxx \
-e HOST=2011101.xyz \
-e PHP_CLI_SERVER_WORKERS=10 \
--name=zerotierhost registry.cn-hangzhou.aliyuncs.com/jcleng/zerotierhost:latest
```
